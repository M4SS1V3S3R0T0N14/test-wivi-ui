import { Category } from '../storiesHierarchy'
import ICONS from './docs/ICONS.md'

export default {
  title: Category.ICONS,
  parameters: {
    viewport: false,
    options: { showAddonPanel: false },
    readme: { content: ICONS }
  }
}

export const Icones = () => ({})
