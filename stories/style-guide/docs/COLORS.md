# Couleurs

La couleur est un élément essentiel de tout système de conception, mais elle peut facilement échapper à tout contrôle. Avec un nombre apparemment infini de teintes et de nuances, la variabilité de différents espaces colorimétriques et diverses méthodes de sélection et d'échantillonnage des couleurs, il est facile pour les équipes de se retrouver avec des dizaines de valeurs de couleur qui sont utilisées de manière incohérente.

En définissant un système de couleurs, vous pouvez vous assurer d'avoir une palette contrôlée (et consolidée) de couleurs acceptables, une utilisation cohérente et appropriée, et également améliorer la facilité de maintenance.

### Application
| Nom          | Variable CSS           | Valeur par défaut               |
|--------------|------------------------|---------------------------------|
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #212429; border: 1px solid lightgrey;"></span> Gris 5        | `--w-color-app-5`        | `#212429`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #495057; border: 1px solid lightgrey;"></span> Gris 4       | `--w-color-app-4`       | `#495057`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #ACB5BD; border: 1px solid lightgrey;"></span> Gris 3       | `--w-color-app-3`       | `#ACB5BD`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #DDE2E5; border: 1px solid lightgrey;"></span> Gris 2       | `--w-color-app-2`       | `#DDE2E5`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #F8F9FA; border: 1px solid lightgrey;"></span> Gris 1       | `--w-color-app-1`       | `#F8F9FA`                      |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Gris 0       | `--w-color-app-0`       | `#FFFFFF`                      |

### Principale
| Nom          | Variable CSS           | Valeur par défaut               |
|--------------|------------------------|---------------------------------|
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #7048E8; border: 1px solid lightgrey;"></span> Principale    | `--w-color-primary`      | `#7048E8`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #B7A3F3; border: 1px solid lightgrey;"></span> Principale (plus claire)    | `--w-color-primary-li`      | `#B7A3F3`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #5028C6; border: 1px solid lightgrey;"></span> Principale (plus sombre)     | `--w-color-primary-da`      | `#5028C6`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste  | `--w-color-on-primary`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus claire)   | `--w-color-on-primary-li`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus sombre)   | `--w-color-on-primary-da`      | `#FFFFFF`                       |

### Secondaire
| Nom          | Variable CSS           | Valeur par défaut               |
|--------------|------------------------|---------------------------------|
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #e8cd48; border: 1px solid lightgrey;"></span> Secondaire    | `--w-color-secondary`      | `#e8cd48`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #f3e6a3; border: 1px solid lightgrey;"></span> Secondaire (+ claire)    | `--w-color-secondary-li`      | `#f3e6a3`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #c6ac28; border: 1px solid lightgrey;"></span> Secondaire (+ foncé)    | `--w-color-secondary-da`      | `#c6ac28`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste   | `--w-color-on-secondary`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus claire)   | `--w-color-on-secondary-li`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus sombre)   | `--w-color-on-secondary-da`      | `#FFFFFF`                       |

### Notification
| Nom          | Variable CSS           | Valeur par défaut               |
|--------------|------------------------|---------------------------------|
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #D84910; border: 1px solid lightgrey;"></span> Notification    | `--w-color-notification`      | `#D84910`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FBEDE7; border: 1px solid lightgrey;"></span> Notification (+ claire)    | `--w-color-notification-li`      | `#FBEDE7`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #821D06; border: 1px solid lightgrey;"></span> Notification (+ foncé)    | `--w-color-notification-da`      | `#821D06`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste   | `--w-color-on-notification`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus claire)   | `--w-color-on-notification-li`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus sombre)   | `--w-color-on-notification-da`      | `#FFFFFF`                       |

### Succès
| Nom          | Variable CSS           | Valeur par défaut               |
|--------------|------------------------|---------------------------------|
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #374FC7; border: 1px solid lightgrey;"></span> Succès    | `--w-color-success`      | `#374FC7`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #EBEDF9; border: 1px solid lightgrey;"></span> Succès (+ claire)    | `--w-color-success-li`      | `#EBEDF9`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #11187E; border: 1px solid lightgrey;"></span> Succès (+ foncé)    | `--w-color-success-da`      | `#11187E`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste   | `--w-color-on-success`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus claire)   | `--w-color-on-success-li`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus sombre)   | `--w-color-on-success-da`      | `#FFFFFF`                       |


### Erreur
| Nom          | Variable CSS           | Valeur par défaut               |
|--------------|------------------------|---------------------------------|
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #F03D3E; border: 1px solid lightgrey;"></span> Erreur    | `--w-color-error`      | `#F03D3E`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FDECEC; border: 1px solid lightgrey;"></span> Erreur (+ claire)    | `--w-color-error-li`      | `#FDECEC`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #9F1F1F; border: 1px solid lightgrey;"></span> Erreur (+ foncé)    | `--w-color-error-da`      | `#9F1F1F`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste   | `--w-color-on-error`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus claire)   | `--w-color-on-error-li`      | `#FFFFFF`                       |
| <span style="display: inline-block; vertical-align: middle; width: 16px; height: 16px; border-radius: 4px; background: #FFF; border: 1px solid lightgrey;"></span> Contraste (plus sombre)   | `--w-color-on-error-da`      | `#FFFFFF`                       |



