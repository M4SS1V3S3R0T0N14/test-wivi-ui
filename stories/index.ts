/**
 * Storybook list of stories
 *
 * order is reflected in storybook sidebar
 */
export default [
  // Introduction
  require('./introduction/GettingStarted.story'),
  require('./introduction/Contribution.story'),

  // Style Guide
  require('./style-guide/GridSpacing.story'),
  require('./style-guide/Typography.story'),
  require('./style-guide/Colors.story'),
  require('./style-guide/Icons.story'),

  // Component library
  require('./component-libray/button/Button.story')
  // require('./component-library/cards/Cards.story'),
  // require('./component-library/collapse/Collapse.story'),
  // require('./component-library/image/Image.story'),
  // require('./component-library/form-controls/Input.story'),
  // require('./component-library/tag/Tag.story'),

  // Pattern library
 // require('./pattern-library/points-of-interest/SinglePoi.story')
]
