import { shallowMount, Wrapper } from '@vue/test-utils'
import { WButton } from '@/components/button'

let wrapper: Wrapper<WButton>

describe('WMyComponent', () => {

  beforeEach(() => {
    wrapper = shallowMount(WButton)
  })

  it('is called', () => {
    expect(wrapper).toBeTruthy()
    expect(wrapper.name()).toBe('w-button')
  })

  it('render correctly', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })

})
