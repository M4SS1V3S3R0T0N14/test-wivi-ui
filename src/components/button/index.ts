import type { PluginObject } from 'vue'

import { use } from '@/utils/plugins'
import Button from './Button.vue'

// Individually plugin install function
const Plugin: PluginObject<any> = {
  install (Vue) {
    Vue.component('w-button', Button)
  }
}

// Auto install library outside of a module system, that it will
// automatically install itself without the need to call Vue.use()
use(Plugin)

// Default export is component, registered via Vue.use()
export default Plugin
// To allow individual component use, export components
// each can be registered via Vue.component()
export { Button as WButton }
