import type { PluginObject } from 'vue'

declare const WiviUI: PluginObject<any>

export default WiviUI
export * from './lib/components'
