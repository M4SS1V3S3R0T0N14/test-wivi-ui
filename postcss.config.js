module.exports = {
  plugins: {
    'postcss-fixes': {
      preset: 'safe'
    },
    /*
    'postcss-css-variables': {
      preserve: true
    },
    */
    'postcss-em-media-query': {},
    'postcss-pxtorem': {
      rootValue: 16,
      unitPrecision: 5,
      propList: ['*'],
      selectorBlackList: [],
      replace: true,
      mediaQuery: false,
      minPixelValue: 0,
      exclude: /node_modules/i
    },
    'cssnano': {
      preset: ['default', {
        calc: true,
        discardDuplicates: true,
        zindex: false,
        mergeRules: true,
        reduceTransforms: false,
        autoprefixer: true,
        colormin: false,
        discardComments: {
          removeAll: true
        }
      }]
    },
  }
}
