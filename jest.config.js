module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  roots: ['./src'],
  testMatch: ['**/*.spec.[jt]s?(x)']
}
