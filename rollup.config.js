import fs from 'fs'
import path from 'path'

import resolve from '@rollup/plugin-node-resolve'
import replace from '@rollup/plugin-replace'
import postcss from 'rollup-plugin-postcss'
import vue from 'rollup-plugin-vue'
import commonjs from '@rollup/plugin-commonjs'
import typescript from 'rollup-plugin-typescript2'
import babel from '@rollup/plugin-babel'
import { terser } from 'rollup-plugin-terser'

import pkg from './package.json'

const LIB_NAME = 'WiviUI'
const BASE_FOLDER = 'src/'
const COMPONENTS_FOLDER = 'components/'

const babelConfig = {
  babelHelpers: 'runtime',
  extensions: ['.js', '.jsx', '.vue', '.ts'],
  exclude: 'node_modules/**'
}

const projectRoot = path.resolve(__dirname, '..')

// ESM/UMD/IIFE shared settings: externals
// Refer to https://rollupjs.org/guide/en/#warning-treating-module-as-external-dependency
const external = [
  // list external dependencies, exactly the way it is written in the import statement.
  ...Object.keys(pkg.peerDependencies || {})
]

// UMD/IIFE shared settings: output.globals
// Refer to https://rollupjs.org/guide/en#output-globals for details
const globals = {
  // Provide global variable names to replace your external imports
  vue: 'Vue'
}

const componentNames = fs
  .readdirSync(BASE_FOLDER + COMPONENTS_FOLDER)
  .filter((f) =>
    fs.statSync(path.join(BASE_FOLDER + COMPONENTS_FOLDER, f)).isDirectory()
  )

const components = componentNames.reduce((obj, name) => {
  obj[name] = (BASE_FOLDER + COMPONENTS_FOLDER + name)
  return obj
}, {})

const basePlugins = [
  resolve(),
  commonjs(),
  typescript({
    typescript: require('typescript'),
    useTsconfigDeclarationDir: true
  }),
  vue({
    css: false,
    template: {
      isProduction: true
    }
  })
]

const esmPlugins = [
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    'process.env.ES_BUILD': JSON.stringify('true'),
  }),
  ...basePlugins
]

const iifePlugins = [
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    'process.env.ES_BUILD': JSON.stringify('false'),
  }),
  ...basePlugins
]

const generateBundle = (entry, format, filename, plugins = []) => ({
  input: entry,
  output: [
    {
      name: LIB_NAME,
      exports: 'named',
      format: format,
      file: filename,
      globals
    }
  ],
  plugins,
  external,
})

const generateStyles = (postcssConfig) => ([
  postcss(postcssConfig)
])

const fullBundle = [
  generateBundle('src/index.ts', 'esm',  'lib/wivi-ui.js', [
    ...esmPlugins,
    ...generateStyles({ extract: 'wivi-ui.css' }),
    babel(babelConfig)
  ]),
  generateBundle('src/index.ts', 'es',  'lib/wivi-ui.min.js', [
    ...esmPlugins,
    ...generateStyles({ extract: 'wivi-ui.min.css', minimize: true }),
    babel(babelConfig),
    terser({ output: { comments: false } })
  ]),
  generateBundle('src/index.ts', 'iife', 'dist/wivi-ui.js', [
    ...iifePlugins,
    ...generateStyles({ extract: 'wivi-ui.css' }),
    babel(babelConfig)
  ]),
  generateBundle('src/index.ts', 'iife', 'dist/wivi-ui.min.js', [
    ...iifePlugins,
    ...generateStyles({ extract: 'wivi-ui.min.css', minimize: true }),
    babel(babelConfig),
    terser({ output: { comments: false } })
  ])
]

const allComponents = Object.keys(components)
  .map((key) => [
    generateBundle(`${components[key]}/index.ts`, 'esm',  `lib/components/${key}.js`, [
      ...esmPlugins,
      ...generateStyles({ extract: `${key}.css` }),
      babel(babelConfig)
    ]),
    generateBundle(`${components[key]}/index.ts`, 'esm',  `lib/components/${key}.min.js`, [
      ...esmPlugins,
      ...generateStyles({ extract: `${key}.min.css`, minimize: true }),
      babel(babelConfig),
      terser({ output: { comments: false } })
    ]),
    generateBundle(`${components[key]}/index.ts`, 'iife', `dist/components/${key}.js`, [
      ...iifePlugins,
      ...generateStyles({ extract: `${key}.css` }),
      babel(babelConfig)
    ]),
    generateBundle(`${components[key]}/index.ts`, 'iife', `dist/components/${key}.min.js`, [
      ...iifePlugins,
      ...generateStyles({ extract: `${key}.min.css`, minimize: true }),
      babel(babelConfig),
      terser({ output: { comments: false } })
    ]) ])

export default [
  ...fullBundle,
  ...[].concat.apply([], allComponents)
]
